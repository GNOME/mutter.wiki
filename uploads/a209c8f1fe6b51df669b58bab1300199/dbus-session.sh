#!/usr/bin/bash

LIGHT_GRAY="\[\033[1m\]"
NO_COLOR="\[\033[0m\]"
export PS1="[$LIGHT_GRAY D-Bus \$(echo \$DBUS_SESSION_BUS_ADDRESS | sed -e 's/.*guid=\([a-z0-9]\{4\}\).*$/\1/') $NO_COLOR][\u@\h \W]$ "

ENV_FILE="$XDG_RUNTIME_DIR/nested-dbus-session.txt"

if [ "x$1" = "x-x" ];then
  export DBUS_SESSION_BUS_ADDRESS=$(cat $ENV_FILE)
  bash -i
elif [ "x$1" = "x-n" ];then
  cat > /tmp/dbussessionbashrc << __EOF__
. ~/.bashrc
echo \$DBUS_SESSION_BUS_ADDRESS > $ENV_FILE
__EOF__

  dbus-run-session -- bash --init-file /tmp/dbussessionbashrc -i
else
  echo "Usage: $0 [-x|-n]"
  exit 1
fi
